const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const schema = require('./schema/schema');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
mongoose.connect('mongodb://username:password@databaseIP:databasePort/databasename');

app.use(cors());
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql:true
}));



app.listen(4000, () => {
    console.log('grapgql-playlist listing on port 4000');
});