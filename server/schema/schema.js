const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLScalarType
} = require('graphql');

const Component = require('../models/Component')
const Children = require('../models/Children');

const { userInput } = require('../models/inputTypes/ChildrenInputType');
const { uiPropertyInput } = require('../models/inputTypes/UiPropertyInputType');

const ComponentType = new GraphQLObjectType({
    name: 'Component',
    fields: () => ({
        id: { type: GraphQLID },
        type: { type: GraphQLString },
        subType: { type: GraphQLString },
        children: {
            type: new GraphQLList(ChildrenType),
            resolve(parent, args) {
                const c = Children.find({ componentId: parent.id });
                console.log('parent:    ', c);
                return c;
            }
        }
    })
});

const ChildrenType = new GraphQLObjectType({
    name: 'Children',
    fields: () => ({
        componentId: { type: GraphQLString },
        id: { type: GraphQLID },
        type: { type: GraphQLString },
        subType: { type: GraphQLString },
        uiProperties: { type: UIPropertiesType }
    })
});

const UIPropertiesType = new GraphQLObjectType({
    name: 'UIProperties',
    fields: () => ({
        id: { type: GraphQLID },
        margin: { type: MarginType },
        dimension: { type: DimensionType },
        decoration: { type: DecorationType }
    })
});

const MarginType = new GraphQLObjectType({
    name: 'Margin',
    fields: () => ({
        id: { type: GraphQLID },
        top: { type: GraphQLInt },
        bottom: { type: GraphQLInt },
    })
});

const DimensionType = new GraphQLObjectType({
    name: 'Dimension',
    fields: () => ({
        id: { type: GraphQLID },
        dimension: { type: WidthType }
    })
});

const WidthType = new GraphQLObjectType({
    name: 'Width',
    fields: () => ({
        id: { type: GraphQLID },
        type: { type: GraphQLString },
        value: { type: GraphQLInt }
    })
});

const DecorationType = new GraphQLObjectType({
    name: 'Decoration',
    fields: () => ({
        id: { type: GraphQLID },
        shape: { type: ShapeType },
        background: { type: BackgroundType }
    })
});

const ShapeType = new GraphQLObjectType({
    name: 'Shape',
    fields: () => ({
        id: { type: GraphQLID },
        type: { type: GraphQLString }
    })
});

const BackgroundType = new GraphQLObjectType({
    name: 'Background',
    fields: () => ({
        id: { type: GraphQLID },
        elevation: { type: GraphQLInt },
        bgColor: { type: GraphQLString }
    })
});
const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        component: {
            type: ComponentType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args){
                return Component.findById(args.id);
            }
        }
    }
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        component: {
            type: ComponentType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                type: { type: new GraphQLNonNull(GraphQLString) },
                subType: { type: new GraphQLNonNull(GraphQLString) },
                children: { type: userInput }
            },
            resolve(parent, args) {
                let component = Component({
                    id: args.id,
                    type: args.type,
                    subType: args.subType,
                    componentId:args.componentId
                });
                args.children.forEach(element => {
                    let children = Children({
                        componentId: element.componentId,
                        id: element.id,
                        type: element.id,
                        subType: element.subType,
                        uiProperties: element.uiProperties
                    });
                    children.save();
                });
                return component.save();
            }
        },
        children: {
            type: ChildrenType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLID) },
                type: { type: new GraphQLNonNull(GraphQLString) },
                componentId: { type: GraphQLString },
                subType: { type: new GraphQLNonNull(GraphQLString) },
                uiProperties: { type: uiPropertyInput }
            },
            resolve(parent, args) {
                let children = Children({
                    componentId: args.componentId,
                    id: args.id,
                    type: args.id,
                    subType: args.subType,
                    uiProperties: args.uiProperties
                });
                return children.save();
            }
        }
    }
});





module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});