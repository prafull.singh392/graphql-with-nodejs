const { Schema,model } = require('mongoose');



const marginSchema = new Schema({
    top: Number,
    bottom: Number
});
const width = new Schema({
    type: String,
    value: Number
});
const shape = new Schema({
    type: String
});
const background = new Schema({
    elevation: Number,
    bgColor: String
});

const decoration = new Schema({
    shape,
    background
});
const dimension = new Schema({
    width
});
const uiProperties = new Schema({
    margin: marginSchema,
    dimension,
    decoration
});

const ChidrenSchema = new Schema({
    componentId: String,
    id: String,
    type: String,
    subType: String,
    uiProperties
});

module.exports = model('Children', ChidrenSchema)