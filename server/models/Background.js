const { Schema,model } = require('mongoose');

const backgroundSchema = new Schema({
    elevation: Number,
    bgColor: String,
});


module.exports = model('Background', backgroundSchema);