const { Schema,model } = require('mongoose');

const widthSchema = new Schema({
    type: String,
    value: Number
});


module.exports = model('Width', widthSchema);