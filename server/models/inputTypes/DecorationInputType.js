const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
  }= require('graphql');
const { backgroundInputType } = require('./BackgroundInputType');
const { shapeInputType } = require('./ShapeInputType');


  
const DecorationInputType = new GraphQLInputObjectType({
    name: 'decorationInputType',
    description: 'Input payload for margin',
    fields: () => ({
        shape: {
            type: shapeInputType
        },
        background: {
            type: backgroundInputType
        }
    }),
});
  
module.exports = {
    decorationInputType: DecorationInputType
}