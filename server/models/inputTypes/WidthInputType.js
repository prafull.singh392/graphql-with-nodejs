const {
    GraphQLInputObjectType, GraphQLInt, GraphQLString,
  }= require('graphql');

  
const WidthInputType = new GraphQLInputObjectType({
    name: 'widthInputType',
    description: 'Input payload for margin',
    fields: () => ({
        type: {
            type: GraphQLString,
        },
        value: {
            type: GraphQLInt,
        }
    }),
});
  
module.exports = {
    widthInputType: WidthInputType
}