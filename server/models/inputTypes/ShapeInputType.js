const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
  }= require('graphql');


  
const ShapeInputType = new GraphQLInputObjectType({
    name: 'ShapeInputType',
    description: 'Input payload for margin',
    fields: () => ({
        type: {
            type: GraphQLString
        }
    }),
});
  
module.exports = {
    shapeInputType: ShapeInputType
}