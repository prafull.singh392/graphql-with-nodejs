const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
  }= require('graphql')
  
  const MarginInputType = new GraphQLInputObjectType({
    name: 'MarginInputType',
    description: 'Input payload for margin',
    fields: () => ({
        top: {
            type: GraphQLInt,
        },
        bottom: {
            type: GraphQLInt,
        }
    }),
  });
  
module.exports = {
    marginInput: MarginInputType
}