const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
  }= require('graphql');
const { uiPropertyInput } = require('./UiPropertyInputType');
const UiPropertyInputType = require('./UiPropertyInputType');
  
  const ChildrenInputType = new GraphQLInputObjectType({
    name: 'CreateUserInput',
    description: 'Input payload for creating user',
    fields: () => ({
      componentId: {
        type: GraphQLString
      },
      id: {
        type: new GraphQLNonNull(GraphQLString),
      },
      type: {
        type: GraphQLString,
      },
      subType: {
        type: GraphQLString,
      },
      uiProperties:{
        type:uiPropertyInput
      }
    }),
  });
  
module.exports = {
    userInput: new GraphQLList(ChildrenInputType)
}