const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
    GraphQLInt,
  }= require('graphql');
const { decorationInputType } = require('./DecorationInputType');
const { dimensionInputType } = require('./DimensionInputType');
const { marginInput } = require('./MarginInputType');


  
const BackgroundInputType = new GraphQLInputObjectType({
  name: 'BackgroundInputType',
  description: 'Input payload for margin',
  fields: () => ({
    elevation: {
      type: GraphQLInt,
    },
    bgColor: {
      type: GraphQLString
    }
  }),
});
  
module.exports = {
    backgroundInputType: BackgroundInputType
}