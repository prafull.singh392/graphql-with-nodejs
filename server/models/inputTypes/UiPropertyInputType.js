const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
  }= require('graphql');
const { decorationInputType } = require('./DecorationInputType');
const { dimensionInputType } = require('./DimensionInputType');
const { marginInput } = require('./MarginInputType');


  
const UiPropertyInputType = new GraphQLInputObjectType({
  name: 'uiPropertyInput',
  description: 'Input payload for margin',
  fields: () => ({
    margin: {
      type: marginInput,
    },
    dimension: {
      type: dimensionInputType
    },
    decoration: {
      type: decorationInputType
    }
  }),
});
  
module.exports = {
    uiPropertyInput: UiPropertyInputType
}