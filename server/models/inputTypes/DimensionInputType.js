const {
    GraphQLString,
    GraphQLInputObjectType,
    GraphQLNonNull,
    GraphQLList,
  }= require('graphql');
const { widthInputType } = require('./WidthInputType');


  
  const DimensionInputType = new GraphQLInputObjectType({
    name: 'dimensionInputType',
    description: 'Input payload for margin',
    fields: () => ({
        width: {
            type: widthInputType,
        }
    }),
  });
  
module.exports = {
  dimensionInputType: DimensionInputType
}