const { Schema,model } = require('mongoose');
const Children = require('./Children');
const { userInput } = require('./inputTypes/ChildrenInputType');

const componentSchema = new Schema({
    id: String,
    type: String,
    subType: String 
});


module.exports = model('component', componentSchema);